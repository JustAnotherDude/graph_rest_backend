<?php


namespace app\components;


use yii\base\Object;
use yii\data\Pagination;


class Paginator extends Object
{
    private $_queryForPagination;
    private $_pagination;
    
    public function getQueryForPagination()
    {
        return $this->_queryForPagination;
    }
    
    public function setQueryForPagination($query)
    {
        $this->_queryForPagination = $query;
    }
    
    public function init()
    {
        $count = $this->_queryForPagination->count();
        
        $this->_pagination = new Pagination(['totalCount' => $count]);
    }
    
    public function responseWithPagination()
    {
        $formattedResp = [];
        
        $formattedResp['data'] = $this->getPaginatedArr();
        $formattedResp['meta'] = [
            'page_count' => $this->_pagination->pageCount,
            'links' => $this->_pagination->getLinks(true),
        ];
        
        return $formattedResp;
    }
    
    private function getPaginatedArr()
    {
        return $this->_queryForPagination
            ->offset($this->_pagination->offset)
            ->limit($this->_pagination->limit)
            ->all();
    }
}