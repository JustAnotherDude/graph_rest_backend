<?php

return [
    'enablePrettyUrl' => true,
    'showScriptName' => false,
    'rules' => [
        'GET graphs' => 'main/index',
        'POST graphs' => 'main/create',
        'POST,PUT graphs/<id:\d+>' => 'main/update',
        'DELETE graphs/<id:\d+>' => 'main/delete',
        'graphs/<id>' => 'main/options',
        'graphs' => 'main/options',
        
        'POST graphs/<id:\d+>/search' => 'main/search',
        'graphs/<id>/search' => 'main/options',
        
        'GET graphs/<graphId:\d+>/vertices' => 'vertex/index',
        'POST graphs/<graphId:\d+>/vertices' => 'vertex/create',
        'POST,PUT graphs/<graphId:\d+>/vertices/<id:\d+>' => 'vertex/update',
        'DELETE graphs/<graphId:\d+>/vertices/<id:\d+>' => 'vertex/delete',
        'graphs/<id>/vertices' => 'vertex/options',
        'graphs/<id>/vertices/<vertId>' => 'vertex/options',
        
        'GET graphs/<graphId:\d+>/ribs' => 'rib/index',
        'POST graphs/<graphId:\d+>/ribs' => 'rib/create',
        'POST,PUT graphs/<graphId:\d+>/ribs/<id:\d+>' => 'rib/update',
        'DELETE graphs/<graphId:\d+>/ribs/<id:\d+>' => 'rib/delete',
        'graphs/<id>/ribs' => 'rib/options',
        'graphs/<id>/ribs/<ribId>' => 'rib/options',
    ],
];
