<?php

namespace app\models;

use app\components\GraphTool;

class Graph extends BaseModel
{
    public static function tableName()
    {
        return '{{graphs}}';
    }
    
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'string', 'max' => 50],
            [['name'], 'unique'],
        ];
    }
    
    public function beforeValidate()
    {
        $this->name = mb_strtolower($this->name);
        
        return parent::beforeValidate();
    }
    
    public function findPathAndCost($arrayWithPoints)
    {
        $graphTool = new GraphTool();
        $fromVertex = Vertex::findByNameOrNotFound($arrayWithPoints['from_vertex']);
        $toVertex = Vertex::findByNameOrNotFound($arrayWithPoints['to_vertex']);
        
        $graphTool->reformatBaseArray($this->getAllRibs()->all());
        return $graphTool->findShortestPath($fromVertex->id, $toVertex->id);
    }
    
    public function getAllRibs()
    {
        return (new \yii\db\Query())
            ->from('graphs')
            ->select('ribs.*')
            ->distinct()
            ->innerJoin('vertices', 'graphs.id = vertices.graph_id')
            ->innerJoin('ribs', 'vertices.id = ribs.start_vertex_id')
            ->where('graphs.id = :graphs', [':graphs' => $this->id]);
    }
    
    public function getVertices()
    {
        return $this->hasMany(Vertex::className(), ['graph_id' => 'id']);
    }
}
