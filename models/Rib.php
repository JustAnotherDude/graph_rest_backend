<?php

namespace app\models;

class Rib extends BaseModel
{
    public static function tableName()
    {
        return '{{ribs}}';
    }
    
    public function rules()
    {
        return [
            [['weight', 'start_vertex_id', 'end_vertex_id'], 'integer'],
            [['start_vertex_id', 'end_vertex_id'], 'unique', 'targetAttribute' => ['start_vertex_id', 'end_vertex_id']],
        ];
    }
    
    public function getStartVertex()
    {
        return $this->hasOne(Vertex::className(), ['id' => 'start_vertex_id']);
    }
    
    public function getEndVertex()
    {
        return $this->hasOne(Vertex::className(), ['id' => 'end_vertex_id']);
    }
}
