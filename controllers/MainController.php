<?php

namespace app\controllers;

use app\components\Paginator;
use app\models\Graph;
use Yii;

class MainController extends BaseController
{
    public function actions()
    {
        $actions = parent::actions();
        $actions['error'] = [
            'class' => 'yii\web\ErrorAction',
        ];
        
        return $actions;
    }
    
    public function actionIndex()
    {
        $paginator = new Paginator(['queryForPagination' => Graph::find()]);
        
        return $paginator->responseWithPagination();
    }
    
    public function actionCreate()
    {
        $model = new Graph();
        $model->attributes = Yii::$app->request->post();
        $response = Yii::$app->response;
        
        if ($model->save()) {
            return $model;
        } else {
            $response->statusCode = 400;
            return $model->getErrors();
        }
    }
    
    public function actionUpdate($id)
    {
        $graph = Graph::findOrNotFound($id);
        $graph->attributes = Yii::$app->request->post();
        $response = Yii::$app->response;
        
        if ($graph->update()) {
            return $graph;
        } else {
            $response->statusCode = 400;
            return $graph->getErrors();
        }
    }
    
    public function actionDelete($id)
    {
        $model = Graph::findOrNotFound($id);
        
        $model->delete();
        return $model;
    }
    
    public function actionSearch($id)
    {
        $graph = Graph::findOrNotFound($id);
        return $graph->findPathAndCost(Yii::$app->request->post());
    }
}
