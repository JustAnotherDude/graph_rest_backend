<?php

namespace app\controllers;

use yii\filters\ContentNegotiator;
use yii\rest\OptionsAction;
use yii\web\Controller;
use yii\web\Response;

class BaseController extends Controller
{
    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors[] = [
            'class' => ContentNegotiator::className(),
            'formats' => ['application/json' => Response::FORMAT_JSON],
        ];
        
        $behaviors['corsFilter'] = [
            'class' => \yii\filters\Cors::className(),
            'cors' => [
                // restrict access to
                'Origin' => ['*'],
                'Access-Control-Request-Method' => ['POST', 'GET', 'PUT', 'OPTIONS', 'DELETE'],
                // Allow only POST and PUT methods
                'Access-Control-Request-Headers' => ['*'],
                // Allow only headers 'X-Wsse'
//                'Access-Control-Allow-Credentials' => true,
                // Allow OPTIONS caching
                'Access-Control-Max-Age' => 3600,
                // Allow the X-Pagination-Current-Page header to be exposed to the browser.
                'Access-Control-Expose-Headers' => ['X-Pagination-Current-Page'],
            ],
        ];
        
        
        return $behaviors;
    }
    
    public function actions()
    {
        return [
            'options' => [
                'class' => OptionsAction::className(),
            ],
        ];
    }
}
