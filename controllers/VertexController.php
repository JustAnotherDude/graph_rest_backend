<?php

namespace app\controllers;

use app\models\Graph;
use app\models\Vertex;
use Yii;

class VertexController extends BaseController
{
    public function actionIndex($graphId)
    {
        $graph = Graph::findOrNotFound($graphId);
        
        return Vertex::belongsToGraph($graph)->all();
    }
    
    public function actionCreate($graphId)
    {
        $graph = Graph::findOrNotFound($graphId);
        
        $vertex = new Vertex();
        $vertex->setSecuredParams(Yii::$app->request->post());
        $vertex->link('graph', $graph);
        $response = Yii::$app->response;
        
        if ($vertex->save()) {
            return $vertex;
        } else {
            $response->statusCode = 400;
            return $vertex->getErrors();
        }
    }
    
    public function actionUpdate($graphId, $id)
    {
        Graph::findOrNotFound($graphId);
        $vertex = Vertex::findOrNotFound($id);
        
        $vertex->setSecuredParams(Yii::$app->request->post());
        $response = Yii::$app->response;
        
        if ($vertex->update()) {
            return $vertex;
        } else {
            $response->statusCode = 400;
            return $vertex->getErrors();
        }
    }
    
    public function actionDelete($graphId, $id)
    {
        Graph::findOrNotFound($graphId);
        $vertex = Vertex::findOrNotFound($id);
        
        $vertex->delete();
        
        return $vertex;
    }
}
