<?php

namespace app\controllers;

use app\models\Graph;
use app\models\Rib;
use Yii;

class RibController extends BaseController
{
    public function actionIndex($graphId)
    {
        $graph = Graph::findOrNotFound($graphId);
        
        return $graph->getAllRibs()->all();
    }
    
    public function actionCreate($graphId)
    {
        Graph::findOrNotFound($graphId);
        $rib = new Rib();
        $response = Yii::$app->response;
        
        $rib->attributes = Yii::$app->request->post();
        
        if ($rib->save()) {
            return $rib;
        } else {
            $response->statusCode = 400;
            return $rib->getErrors();
        }
    }
    
    public function actionUpdate($graphId, $id)
    {
        Graph::findOrNotFound($graphId);
        $rib = Rib::findOrNotFound($id);
        $response = Yii::$app->response;
        
        $rib->attributes = Yii::$app->request->post();
        
        if ($rib->update()) {
            return $rib;
        } else {
            $response->statusCode = 400;
            return $rib->getErrors();
        }
    }
    
    public function actionDelete($graphId, $id)
    {
        $rib = Rib::findOrNotFound($id);
        $rib->delete();
        
        return $rib;
    }
}
