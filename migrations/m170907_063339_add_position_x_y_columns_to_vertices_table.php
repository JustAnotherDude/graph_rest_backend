<?php

use yii\db\Migration;

/**
 * Handles adding position_x_y to table `vertices`.
 */
class m170907_063339_add_position_x_y_columns_to_vertices_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn(
            'vertices',
            'x',
            $this->integer()->notNull()->defaultValue(0)
        );

        $this->addColumn(
            'vertices',
            'y',
            $this->integer()->notNull()->defaultValue(0)
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
    }
}
