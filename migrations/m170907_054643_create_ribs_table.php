<?php

use yii\db\Migration;

/**
 * Handles the creation of table `ribs`.
 */
class m170907_054643_create_ribs_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('ribs', [
            'id' => $this->primaryKey(),
            'weight' => $this->integer()->notNull(),
            'start_vertex_id' => $this->integer()->notNull(),
            'end_vertex_id' => $this->integer()->notNull(),
        ]);

        $this->createIndex(
            'idx-ribs-start_vertex_id',
            'ribs',
            'start_vertex_id'
        );

        $this->createIndex(
            'idx-ribs-end_vertex_id',
            'ribs',
            'end_vertex_id'
        );

        $this->addForeignKey(
            'fk-ribs-start_vertex_id',
            'ribs',
            'start_vertex_id',
            'vertices',
            'id',
            'CASCADE'
        );

        $this->addForeignKey(
            'fk-ribs-end_vertex_id',
            'ribs',
            'end_vertex_id',
            'vertices',
            'id',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('ribs');
    }
}
