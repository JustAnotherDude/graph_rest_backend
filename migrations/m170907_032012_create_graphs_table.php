<?php

use yii\db\Migration;

/**
 * Handles the creation of table `graphs`.
 */
class m170907_032012_create_graphs_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('graphs', [
            'id' => $this->primaryKey(),
            'name' => $this->text()->notNull()->unique(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('graphs');
    }
}
