<?php

use yii\db\Migration;

/**
 * Handles the creation of table `vertices`.
 */
class m170907_041557_create_vertices_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('vertices', [
            'id' => $this->primaryKey(),
            'name' => $this->text()->notNull()->unique(),
            'graph_id' => $this->integer()->notNull(),
        ]);

        $this->createIndex(
            'idx-vertices-graph_id',
            'vertices',
            'graph_id'
        );

        $this->addForeignKey(
            'fk-verticies-graph_id',
            'vertices',
            'graph_id',
            'graphs',
            'id',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('vertices');
        $this->dropForeignKey('fk-verticies-graph_id', 'vertices');
        $this->dropIndex('idx-vertices-graph_id', 'vertices');
    }
}
